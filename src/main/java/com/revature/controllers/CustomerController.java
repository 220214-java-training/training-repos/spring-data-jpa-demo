package com.revature.controllers;

import com.revature.models.Customer;
import com.revature.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/customers") // add /customer-app to every path in this class
@CrossOrigin // this solves CORS issues in our browser
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping // this method handles GET /customers and GET /customers?name=...
    public List<Customer> getCustomerHandler(@RequestParam(required = false, value = "name") String nameParam){
        System.out.println("Name Parameter: " + nameParam);
        if(nameParam==null){ // GET /customers
            return customerService.getAllCustomers();
        } else { // GET /customers?name=...
            return customerService.getCustomersByName(nameParam);
        }
    }

    @GetMapping("/{id}") // this method handles GET /customers/{id}
    public Customer getCustomerById(@PathVariable("id")int idParam) {
        return customerService.getCustomerById(idParam);
    }

    @PostMapping// this method handles POST /customers (should return a 201)
    public ResponseEntity<Customer> addNewCustomer(@RequestBody Customer newCustomer){
        System.out.println("We got a customer object:  "+newCustomer);
        return new ResponseEntity<>(customerService.addNewCustomer(newCustomer), HttpStatus.CREATED);
    }

}
